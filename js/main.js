$(document).ready(function () {

    // Activate menu on mobile devices
    $('body').click(function (e) {
        var trigger = $('#menuTrigger');
        var menu = $("#menu");

        if (trigger.is(e.target)) {
            trigger.toggleClass('active')
            menu.toggleClass('open');
        }
        else {
            trigger.removeClass('active');
            menu.removeClass('open');
        }
    })

    // Menu item class switch  and scroll to section
    $('#menu > ul > li').click(function () {
        $(this).addClass('active').siblings('li').removeClass('active');
    }).bind('click.smoothscroll', function (e) {
        e.preventDefault();
        var link = $(this).children()[0].getAttribute('href');
        var $link = $(link);

        $('html, body').animate({
            scrollTop: parseInt($link.offset().top)
        }, 500);
    });
});